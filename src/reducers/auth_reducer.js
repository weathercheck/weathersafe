import {
    FIREBASE_LOGIN_SUCCESS,
    FIREBASE_LOGIN_FAIL
} from '../actions/types';

export default function(state = {}, action) {
    switch (action.type) {
        case FIREBASE_LOGIN_SUCCESS:
            return {uid: action.payload};
        case FIREBASE_LOGIN_FAIL:
            return {uid: null};
        default:
            return state;
    }
}