import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";

const NoEventScreen = ({navigation: {navigate}}) =>
        <View style={styles.container}>
            <Image source={require('../assets/bg-1.jpg')}
                   style={styles.background} >
                <Text style={styles.h1}>No Severe Weather</Text>
                <Image source={require('../assets/logo.png')}
                       style={styles.logo} />

                <TouchableOpacity onPress={() => navigate("NoEventScreen")}>
                    <Text>NoEventScreen</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigate("EventScreen")}>
                    <Text>EventScreen</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigate("SafetyScreen")}>
                    <Text>SafetyScreen</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigate("QuestionsScreen")}>
                    <Text>QuestionsScreen</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigate("PhotoScreen")}>
                    <Text>PhotoScreen</Text>
                </TouchableOpacity>
            </Image>
        </View>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    backgroundImage: {
        flexGrow:1,
        height:null,
        width:null,
        alignItems: 'center',
        justifyContent:'center',
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 60,
        fontWeight: 'bold',
        margin: 25,

    },
    logo:{
        marginTop:200
    }
});
export default NoEventScreen;
