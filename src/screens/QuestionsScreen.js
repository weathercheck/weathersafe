import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';

const QuestionsScreen = ({navigation: {navigate}}) =>
    <View>
        <Image source={require('../assets/safe.jpg')}
               style={styles.background} >
            <RaisedTextButton
                onPress={() => navigate("SafetyScreen")}
                title="I AM OKAY"
                color="#3a843a"
                titleColor='white'
                style={styles.button}
            />

            <Image source={require('../assets/logo.png')}
                   style={styles.logo} />
            <RaisedTextButton
                onPress={() => navigate("SafetyScreen")}
                title="I NEED HELP"
                color="#f43c18"
                titleColor='white'
                style={styles.button}
            />
            <TouchableOpacity onPress={() => navigate("EventScreen")}>
                <Text>EventScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("SafetyScreen")}>
                <Text>SafetyScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("QuestionsScreen")}>
                <Text>QuestionsScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("PhotoScreen")}>
                <Text>PhotoScreen</Text>
            </TouchableOpacity>
        </Image>
    </View>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    backgroundImage: {
        flexGrow:1,
        height:null,
        width:null,
        alignItems: 'center',
        justifyContent:'center',
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 60,
        fontWeight: 'bold',
        margin: 20,

    },
    span:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 40,
    },
    logo:{
        marginTop:130
    },
    button:{
        marginTop:175,
        margin: 5,
    }
});


export default QuestionsScreen;
