import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image
} from "react-native";

const SafetyScreen = ({navigation: {navigate}}) =>
    <View style={styles.container}>
        <Image source={require('../assets/bg-1.jpg')}
               style={styles.background} >
            <Text style={styles.h1}>Tips</Text>
            <Text style={styles.p}>Stay away from windows, doors and outside walls. Protect your head. </Text>
            <Text style={styles.p}>In homes and small buildings, go to the basement or to an interior part of the lowest level -- take shelter in closets, bathrooms or interior halls away from windows. Get under something sturdy or lie in the bathtub and cover yourself with a blanket. </Text>
            <Text style={styles.p}>In schools, nursing homes, hospitals, factories, shopping centers and malls, go to pre-designated shelter areas. Interior hallways on the lowest levels are best. Stay away from exterior glass doors. </Text>
            <Text style={styles.p}>In high-rise buildings, go to interior, small rooms or hallways on the lowest floor possible with no windows. If you can see outside, you are not safe. </Text>
            <Text style={styles.p}>In vehicles or mobile homes, vacate and go to a substantial structure. If there is no shelter nearby, lie flat in the nearest ditch, ravine or culvert with your hands shielding your head and neck. </Text>
            <Image source={require('../assets/logo.png')}
                   style={styles.logo} />

            <TouchableOpacity onPress={() => navigate("NoEventScreen")}>
                <Text>NoEventScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("EventScreen")}>
                <Text>EventScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("SafetyScreen")}>
                <Text>SafetyScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("QuestionsScreen")}>
                <Text>QuestionsScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("PhotoScreen")}>
                <Text>PhotoScreen</Text>
            </TouchableOpacity>
        </Image>
    </View>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    backgroundImage: {
        flexGrow:1,
        height:null,
        width:null,
        alignItems: 'center',
        justifyContent:'center',
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 60,
        fontWeight: 'bold',
        margin: 25,
        backgroundColor: 'rgba(0,0,0,0)',

    },
    logo:{
        marginTop:50
    },
    p:{
        color: '#FFFFFF'
    }
});

export default SafetyScreen;
