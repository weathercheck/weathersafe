import React from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';


const EventScreen = ({navigation: {navigate}}) =>
    <View style={styles.container}>
        <Image source={require('../assets/bg-1.jpg')}
               style={styles.background} >
            <Text style={styles.h1}>TORNADO WARNING </Text>
            <Text style={styles.span}>In Your Area</Text>

                <RaisedTextButton
                    onPress={() => navigate("SafetyScreen")}
                    title="I'm Taking Shelter"
                    color="#f43c18"
                    titleColor='white'
                    style={styles.button}
                />

            <Image source={require('../assets/logo.png')}
                   style={styles.logo} />

            <TouchableOpacity onPress={() => navigate("SafetyScreen")}>
                <Text style={styles.button}>NoEventScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("EventScreen")}>
                <Text>EventScreen</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigate("QuestionsScreen")}>
                <Text>QuestionsScreen</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate("PhotoScreen")}>
                <Text>PhotoScreen</Text>
            </TouchableOpacity>
        </Image>
    </View>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    backgroundImage: {
        flexGrow:1,
        height:null,
        width:null,
        alignItems: 'center',
        justifyContent:'center',
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 60,
        fontWeight: 'bold',
        margin: 20,

    },
    span:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 40,
    },
    logo:{
        marginTop:20
    },
    button:{
        marginTop:175,
        margin: 5,
    }
});

export default EventScreen;