import React, { Component } from "react";
import { View, Text ,AsyncStorage } from "react-native";
import { connect } from 'react-redux';
import * as actions from '../actions';

class SplashScreen extends Component {
    componentDidMount(){
        this.props.firebaseLogin();
        console.log(AsyncStorage);
        this.onAuthComplete(this.props)
    }

    onAuthComplete(props){
        if(props.uid){
            this.props.navigation.navigate('QuestionsScreen');
        }
        console.log(this.props);
        console.log(AsyncStorage);
    }

    render(){
        return (
            <View>
                <Text>
                    Auth
                </Text>
            </View>
        )
    }
}

function mapStateToProps( { auth }) {
    return { uid: auth.uid}
}

export default connect(mapStateToProps, actions)(SplashScreen) ;