import React from "react";
import {
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';

const PhotoScreen = ({navigation: {navigate}}) =>
    <Image source={require('../assets/photo.jpg')}
           style={styles.background} >
        <RaisedTextButton
            onPress={() => navigate("SafetyScreen")}
            title="Take Pictures"
            color="#3a843a"
            titleColor='white'
            style={styles.button}
        />
        <Image source={require('../assets/logo.png')}
               style={styles.logo} />
        <TouchableOpacity onPress={() => navigate("EventScreen")}>
            <Text>EventScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate("SafetyScreen")}>
            <Text>SafetyScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate("QuestionsScreen")}>
            <Text>QuestionsScreen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate("PhotoScreen")}>
            <Text>PhotoScreen</Text>
        </TouchableOpacity>
    </Image>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    backgroundImage: {
        flexGrow:1,
        height:null,
        width:null,
        alignItems: 'center',
        justifyContent:'center',
    },
    h1:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 60,
        fontWeight: 'bold',
        margin: 20,

    },
    span:{
        textAlign: 'center',
        fontFamily: 'Cochin',
        fontSize: 40,
    },
    logo:{
        marginTop:130
    },
    button:{
        marginTop:175,
        margin: 5,
    }
});

export default PhotoScreen;
