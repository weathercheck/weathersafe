import React from 'react';
import { AppRegistry  } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';

import store from './store'
import NoEventScreen from "./screens/NoEventScreen";
import EventScreen from "./screens/EventScreen";
import QuestionsScreen from "./screens/QuestionsScreen";
import SafetyScreen from "./screens/SafetyScreen";
import PhotoScreen from "./screens/PhotoScreen";
import SplashScreen from "./screens/SplashScreen";

class WeatherSafe extends React.Component {

    render() {
        const MainNavigator = TabNavigator({
            NoEventScreen : { screen: NoEventScreen },
            EventScreen: { screen: EventScreen },
            QuestionsScreen: { screen: QuestionsScreen },
            SafetyScreen: { screen: SafetyScreen },
            PhotoScreen: { screen: PhotoScreen },
            SplashScreen: { screen: SplashScreen },
        }, {
            navigationOptions: {
                    tabBarVisible: false
                }
            });

        return (
            <Provider store={store}>
                <MainNavigator />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('WeatherSafe', () => WeatherSafe);