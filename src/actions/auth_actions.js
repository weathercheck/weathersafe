import { AsyncStorage } from 'react-native';
import {
    FIREBASE_LOGIN_SUCCESS
} from './types';
import firebase from 'firebase'

export const firebaseLogin = () => async dispatch => {

    firebase.initializeApp({
        apiKey: 'AIzaSyBMzTDOTRbkzajpHZoV8RJN8qznSUQkp-w',
        authDomain: 'weathersafe-be499.firebaseapp.com',
        databaseURL: 'https://weathersafe-be499.firebaseio.com',
        projectId: 'weathersafe-be499',
        storageBucket: 'weathersafe-be499.appspot.com',
        messagingSenderId: '1054352209943'
    });

    let uid = await AsyncStorage.getItem('fb_uid');

    if (uid) {
        // Dispatch an action saying FB login is done
        dispatch({ type: FIREBASE_LOGIN_SUCCESS, payload: uid });
    } else {
        // Start up FB Login process
        doFirebaseLogin(dispatch);
    }
};

const doFirebaseLogin = async dispatch => {
    let user =  await firebase.auth().signInAnonymously();
    console.log(user);
    await AsyncStorage.setItem('fb_uid', user.uid);
    dispatch({ type: FIREBASE_LOGIN_SUCCESS, payload: uid });
};
